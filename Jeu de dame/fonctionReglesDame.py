#-*- coding: utf-8 -*

from fonctionPlateau import longueurPlateau, damier, affichage, plateauDame

def pionJoueurDame (nbrJoueur): #Fonction qui créé un tableau avec les pions des joueurs

	#pion = ["⚪","⚫","2","3","4","5","6","7","8","9"]
	pion = ["0","1","2","3","4","5","6","7","8","9"]
	t2=[]
	for j in range (0, nbrJoueur): #Créé un tableau avec un nbre de pions égal au nbre de joueurs
		t2.append (pion[j])

	return t2

def déplacementSimpleDame(cooY, cooX, cooY2, cooX2, tableau, joueur): #Fonction pour déplacer le pion
	
	tableau[cooY2][cooX2] = joueur #Change la case d'arrivée par le pion du joueur
	tableau[cooY][cooX] = "x" #Change la case de départ par une case vide

	return tableau 

def déplacementComplexeDame(cooY, cooX, cooY2, cooX2, tableau, cooY3, cooX3, joueur): #Fonction pour manger un pion

	tableau[cooY2][cooX2] = joueur #Change la case d'arrivée par le pion du joueur
	tableau[cooY][cooX] = "x" #Change la case de départ par une case vide
	tableau[cooY3][cooX3] = "x" #Change la case du pion adverse par une case vide

	return tableau

def déplacementDame(joueur, adversaire, tableau): #Fonction demandant les coordonnées de déplacement
	print("")
	test = 0
	while test == 0:
		test1 = 0

		while test1 == 0:
			print ("Case de départ :")
			cooY = int(input("Coordonnée de la ligne : "))
			cooX = int(input("Coordonnée de la colonne : "))
			if  str(tableau[cooY][cooX]) != joueur:
				print ("Ce n'est pas un de vos pion.")
				print ("")
			else:
				test1 = 1

		print("")

		test1 = 0
		while test1 == 0:
			print ("Case d'arrivée :")
			cooY2 = int(input("Coordonnée de la ligne : "))
			cooX2 = int(input("Coordonnée de la colonne : "))

			if (int(cooY) - int(cooY2)) %2 == 1: #Si le joueur veux aller à une case de son pion

				if joueur == "0": #Si le joueur possède les "0"

					if int(cooY2) == int(cooY - 1) and int(cooX2) == int(cooX - 1): #Si le joueur veut avancer en haut à gauche
						
						if tableau[cooY2][cooX2] == "x":
							tableau = déplacementSimpleDame(cooY, cooX, cooY2, cooX2, tableau, joueur)
							test = 1
							test1 = 1
						else:
							print ("Case non disponible.")
							test1 = 1

					elif int(cooY2) == int(cooY - 1) and int(cooX2) == int(cooX + 1): #Si le joueur veut avancer en haut à droite
						if tableau[cooY2][cooX2] == "x":
							tableau = déplacementSimpleDame(cooY, cooX, cooY2, cooX2, tableau, joueur)
							test = 1
							test1 = 1
						else:
							print ("Case non disponible.")
							test1 = 1

					else:
						print ("Case non disponible.")
						test1 = 1

				elif joueur == "1":

					if int(cooY2) == int(cooY + 1) and int(cooX2) == int(cooX + 1): #Si le joueur veut avancer en bas à droite
						if tableau[cooY2][cooX2] == "x":
							tableau = déplacementSimpleDame(cooY, cooX, cooY2, cooX2, tableau, joueur)
							test = 1
							test1 = 1
						else:
							print ("Case non disponible.")
							test1 = 1
					
					

					elif int(cooY2) == int(cooY + 1) and int(cooX2) == int(cooX - 1): #Si le joueur veut avancer en haut à gauche
						if tableau[cooY2][cooX2] == "x":
							tableau = déplacementSimpleDame(cooY, cooX, cooY2, cooX2, tableau, joueur)
							test = 1
							test1 = 1
						else:
							print ("Case non disponible.")
							test1 = 1

					else:
						print ("Case non disponible.")
						test1 = 1
				else :
					print ("Case non disponible.")
					test1 = 1
				
			elif (int(cooY) - int(cooY2)) %2 == 0 and (int(cooX) - int(cooX2)) %2 == 0: #Si le joueur veux aller à une case de son pion

				if tableau[cooY2][cooX2] == tableau[cooY + 2][cooX + 2]: #Si le joueur veut manger le pion en bas à droite
					if tableau[cooY + 1][cooX + 1] == adversaire and tableau[cooY2][cooX2] == "x":
						tableau = déplacementComplexeDame(cooY, cooX, cooY2, cooX2, tableau, cooY + 1, cooX + 1, joueur)
						test = 1
						test1 = 1
					else:
						print ("Coordonnées invalide.")
						test1 = 1

				elif tableau[cooY2][cooX2] == tableau[cooY - 2][cooX - 2]: #Si le joueur veut manger le pion en haut à gauche
					if tableau[cooY - 1][cooX - 1] == adversaire and tableau[cooY2][cooX2] == "x":
						tableau = déplacementComplexeDame(cooY, cooX, cooY2, cooX2, tableau, cooY - 1, cooX - 1, joueur)
						test = 1
						test1 = 1
					else:
						print ("Coordonnées invalide.")
						test1 = 1

				elif tableau[cooY2][cooX2] == tableau[cooY + 2][cooX - 2]: #Si le joueur veut manger le pion en bas à gauche
					if tableau[cooY + 1][cooX - 1] == adversaire and tableau[cooY2][cooX2] == "x":
						tableau = déplacementComplexeDame(cooY, cooX, cooY2, cooX2, tableau, cooY + 1, cooX - 1, joueur)
						test = 1
						test1 = 1
					else:
						print ("Coordonnées invalide.")
						test1 = 1

				elif tableau[cooY2][cooX2] == tableau[cooY - 2][cooX + 2]: #Si le joueur veut manger le pion en haut à droite
					if tableau[cooY - 1][cooX + 1] == adversaire and tableau[cooY2][cooX2] == "x":
						tableau = déplacementComplexeDame(cooY, cooX, cooY2, cooX2, tableau, cooY - 1, cooX + 1, joueur)
						test = 1
						test1 = 1
					else:
						print ("Coordonnées invalide.")
						test1 = 1

			else:
				print ("Coordonnées invalide.")
				test1 = 1
	
	affichage(len(tableau), tableau)

	return tableau

def pertePionDame(nbrPion, joueur, tableau): #Enlève les pions aux joueurs
	
	pion1 = 0
	pion2 = 0
	for j in range (0,10):
		for i in range (0,10):
			if tableau[j][i] ==  "⚪":
				pion1 += 1
			elif tableau[j][i] == "⚫":
				pion2 += 1
	nbrPion = [pion1, pion2]
	return nbrPion

def finDame(): #Fonction qui vérifie si la partie est finie

	print ("")