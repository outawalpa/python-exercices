#-*- coding: utf-8 -*

def longueurPlateau (): #Fonction demandant la longueur du plateau
	longueur = 0  #Demande la taille du damier
	while longueur < 5 or longueur > 25: 
		print ("")
		longueur = int(input("Quel est la longueur du plateau (De 5 à 25) ? "))
		if longueur < 5 or longueur > 25:
			print ("Longueur non valide")

	return int(longueur)

def damier (longueur): #Fonction qui créé un damier de cases blanches et noires

	tableau = [] 
	for j in range (0,longueur):
		t = []
		for i in range (0,longueur):
			if i%2==0 and j%2==0:
				t.append(" ")
			elif i%2==0 and not j%2==0:
				t.append("x")
			elif i%2!=0 and j%2==0:
				t.append("x")
			else:
				t.append(" ")
		tableau.append(t)

	#affichage(longueur, tableau)

	return tableau

def affichage(longueur, tableau): #Fonction qui affiche le tableau/plateau proprement

	print ("  ", end = "") # Affiche les coordonnées en haut
	for x in range (0, longueur):
		print (" " + str(x) + " ", end = "")
	print ("")

	for j in range (0,longueur): #Affiche le damier proprement
		print (str(j), end = " ") #Affiche les cordonnées sur le côté
		for i in range (0,longueur):
		
			#Better display with color circle
			if str(tableau[j][i]) == "0":
				case = "⚪"
			elif str(tableau[j][i]) == "1":
				case = "⚫"
			else:
				case = str(tableau[j][i])

			# Display
			if i !=  (longueur - 1):
				print ("[" + case + "]", end = "")
			else:
				print ("[" + case + "]")

def plateauDame (longueur, tableau): #Fonction qui créé un plateau avec les pions

	calcul = (longueur/10)*4 #hauteur sur laquelle je veux mes pions
	taille = int(calcul) #hauteur sans virugule
	calcul2 = calcul - taille #Recupere ce qu'il y a apres la virgule

	if calcul2 < 0.5: #Arrondi ma hauteur au superieur
		taille = taille
	else: #Arrondi ma hauteur à l'inferieur
		taille = taille + 1

	for j in range (0,longueur): #Place les pions sur le damier
		for i in range (0,longueur):
			if tableau[j][i] == "x"  and j < taille:
				#tableau[j][i] = "⚫"
				tableau[j][i] = "1"
			elif tableau[j][i] == "x"  and j >= (longueur - taille):
				#tableau[j][i] = "⚪"
				tableau[j][i] = "0"

	affichage(longueur, tableau)

	return tableau