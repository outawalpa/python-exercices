#-*- coding: utf-8 -*

def nomJoueur (nbrJoueur, minJoueur, maxJoueur): #Fonction qui demande le nbre de joueurs et leurs noms
	if nbrJoueur == 0:
		while nbrJoueur < minJoueur or nbrJoueur > maxJoueur: #Demande le nbre de joueurs
			nbrJoueur = int(input ("Nombre de joueur (Entre " + str(minJoueur) + " et " + str(maxJoueur) + ") : "))
			if nbrJoueur < minJoueur or nbrJoueur > maxJoueur:
				print ("Nombre de joueurs invalide.")

	tableauJoueur = []
	for j in range (0,nbrJoueur): #Demande le nom de chaque joueur
		print ("")
		nom = str(input ("Nom du joueur " + str(j + 1) + " :")) 
		tableauJoueur.append(nom)

	return tableauJoueur

def restart(): #Fonction qui propose une revanche
	restart    = ""
	while str(restart) != "O" or str(restart) != "N":
		print ("")
		restart = (input("Revanche ? (O|N) "))
		if str(restart) == "O":
			return True 
		elif str(restart) == "N":
			return False
		else:
			print ("Reponse non valide.")

def quit(): #Fonction qui propose de relancer
	restart    = ""
	while str(restart) != "O" or str(restart) != "N":
		print ("")
		restart = (input("Retourner au menu ? (O|N) "))
		if str(restart) == "O":
			return True 
		elif str(restart) == "N":
			return False
		else:
			print ("Reponse non valide.")