#-*- coding: utf-8 -*

from fonctionJeuGeneral import nomJoueur, restart, quit
from fonctionPlateau import longueurPlateau, damier, affichage, plateauDame
from fonctionReglesDame import pionJoueurDame, déplacementSimpleDame, déplacementComplexeDame, déplacementDame, pertePionDame, finDame

NbrJoueur = 2 #Nombre de joueur
MinJoueur = 2 #Nombre de joueur minimum
MaxJoueur = 2 #Nombre de joueur maximum
Longueur = 10 #Taille du plateau
nbrPion = [20, 20] #Nombre de pion par joueur
quit = False

while not quit:
	nomJoueur = nomJoueur (NbrJoueur, MinJoueur, MaxJoueur) #Créer le tableau avec le nom des 2 joueurs

	restart = False
	while not restart:
		plateau = plateauDame (Longueur, damier(Longueur)) #Créer le plateau
		pion = pionJoueurDame(NbrJoueur) #Assigne les pions "0" et "1" aux deux joueurs
		actif = 0 #Désigne le premier joueur
		adversaire = 1 #Désigne l'autre joueur
		
		fin = False
		while not fin:
				print ("")
				print ("Au tour de " + nomJoueur[actif] + "(" + pion[actif] + ")") #Appel le joueur actif et montre son pion
				print ("")
				déplacementDame(str(actif), str(adversaire), plateau) #Demande et affiche le déplacement du joueur
				print("")
				pertePionDame(nbrPion, adversaire, plateau)
				actif, adversaire = adversaire, actif #Echange de joueur actif
				#finDame()

	restart = restart() #Demande si les joueurs veulent une revanche
quit = quit() #Demande si les joueurs veulent relancer depuis le début