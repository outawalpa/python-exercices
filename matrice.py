#--------------------------------------------------------------------------
# Affiche la matrice proprement
#--------------------------------------------------------------------------
def affichage(longueur, largeur, tableau):
	for j in range (0,longueur): #Affiche le damier proprement
		for i in range (0,largeur):
			if i !=  (largeur - 1):
				print (str(tableau[j][i]), end = "")
			else:
				print (str(tableau[j][i]))

#--------------------------------------------------------------------------
# Symétrie horizontale
#--------------------------------------------------------------------------
def horizontal(tableau, taille):
	tab = []
	compteur = taille-1
	for i in range (0, taille):
		tab.append(tableau[compteur])
		compteur -= 1
	return tab

#--------------------------------------------------------------------------
# Symétrie verticale
#--------------------------------------------------------------------------
def vertical(tableau, taille):
	tab = []
	for i in range (0, taille):
		test = tableau[i]
		test.reverse()
		tab.append(test)
	return tab

#--------------------------------------------------------------------------
# Symétrie diagonale
#--------------------------------------------------------------------------
def diagonal(tableau, taille):
	tab = []
	tab2 = []
	for i in range (0, taille):
		tab2 = []
		for j in range (0, taille):
			tab2.append(tableau[j][i])
		tab.append(tab2)
	return tab

#--------------------------------------------------------------------------
# Symétrie sens horaire
#--------------------------------------------------------------------------
def sensHoraire(tableau, taille):
	
	tab = diagonal(tableau, taille)
	tab = vertical(tab, taille)

	return tab

#--------------------------------------------------------------------------
# Symétrie sens horaire
#--------------------------------------------------------------------------
def sensAntiHoraire(tableau, taille):
	
	tab = diagonal(tableau, taille)
	tab = horizontal(tab, taille)

	return tab

#--------------------------------------------------------------------------
# Demi tour
#--------------------------------------------------------------------------
def demiTour(tableau, taille):
	
	tab = sensHoraire(tableau, taille)
	tab = sensHoraire(tab, taille)

	return tab

#--------------------------------------------------------------------------
# Menu
#--------------------------------------------------------------------------
def menu():
	recommencer = True

	while recommencer == True:

		array =[list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('       _==/          i     i          \==_       '),
				list('     /XX/            |\___/|            \XX\     '),
				list('   /XXXX\            |XXXXX|            /XXXX\   '),
				list('  |XXXXXX\_         _XXXXXXX_         _/XXXXXX|  '),
				list(' XXXXXXXXXXXxxxxxxxXXXXXXXXXXXxxxxxxxXXXXXXXXXXX '),
				list('|XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|'),
				list('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'),
				list('|XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|'),
				list(' XXXXXX/^^^^"\XXXXXXXXXXXXXXXXXXXXX/^^^^^\XXXXXX '),
				list('  |XXX|       \XXX/^^\XXXXX/^^\XXX/       |XXX|  '),
				list('    \XX\       \X/    \XXX/    \X/       /XX/    '),
				list('       "\       "      \X/      "       /"       '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				list('                                                 '),
				]

		longueur = 49
		largeur = 49

		affichage(longueur, largeur, array)

		quit = False

		while quit == False:

			ask = 0
			while not (ask == '1' or ask == '2' or ask == '3' or ask == '4' or ask == '5' or ask == '6'):

				print ('')
				print ('1. Recommencer\n2. Symétrie verticale\n3. Symétrie horizontale\n4. Sens horaire\n5. Sens anti-horaire\n6. Sens anti-horaire\n7. Quitter')
				print ('')
				ask = input ('Que voulez vous faire ? ')
				print ('')

				if ask == '1':
					quit = True

				if ask == '2':
					array = vertical(array, longueur)
					affichage(longueur, largeur, array)

				if ask == '3':
					array = horizontal(array, longueur)
					affichage(longueur, largeur, array)

				if ask == '4':
					array = sensHoraire(array, longueur)
					affichage(longueur, largeur, array)

				if ask == '5':
					array = sensAntiHoraire(array, longueur)
					affichage(longueur, largeur, array)

				if ask == '6':
					array = demiTour(array, longueur)
					affichage(longueur, largeur, array)

				if ask == '7':
					quit()

#--------------------------------------------------------------------------
# Code
#--------------------------------------------------------------------------
menu()