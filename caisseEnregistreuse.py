nomArticle = []
tvaArticle =[]
prixArticle = []
nbreArticle = []

ticket = False
while ticket == False:
	nom = input("Nom de l'article : ")
	nomArticle.append(nom)
	print ("")
	prix = input ("Prix HT : ")
	prixArticle.append(prix)
	print ("")
	testTva = False
	while testTva == False:
		print ("1. TVA à 20%")
		print ("2. TVA à 10%")
		print ("3. TVA à 5.5%")
		print ("4. TVA à 2.1%")
		tva = str(input ("Montant de la TVA : "))
		if tva == "1" :
			tvaArticle.append(20)
			testTva = True
		elif tva == "2" :
			tvaArticle.append(10)
			testTva = True
		elif tva == "3" :
			tvaArticle.append(5.5)
			testTva = True
		elif tva == "4" :
			tvaArticle.append(2.1)
			testTva = True
		else:
			print ("Réponse invalide")
	print ("")
	nbre = input ("Nombre d'article : ")
	nbreArticle.append(nbre)
	print ("")
	testFin = False
	while testFin == False:
		fin = str(input("Y a-t-il d'autres articles (O|N) ? "))
		if fin == "O":
			ticket = False
			testFin = True
		elif fin == "N":
			ticket = True
			testFin = True
		else:
			print ("Réponse invalide")
	print ("")

prixHTTotal = 0
prixTTCTotal = 0
for i in range (0, len(nomArticle)):
	prixArticle[i] = float(prixArticle[i]) * int(nbreArticle[i])
	prixTTC = prixArticle[i] * (1 + (float(tvaArticle[i]) / 100))
	print (str(nbreArticle[i]) + " " + str(nomArticle[i]) + " . Prix HT = " + str(prixArticle[i]) + "€. Prix TTC = " + str(prixTTC) + "€.")
	print ("")
	prixHTTotal = float(prixHTTotal) + float(prixArticle[i])
	prixTTCTotal = float(prixTTCTotal) + float(prixTTC)
print ("Total HT = " + str(prixHTTotal) + "€. Prix TTC Total = " + str(prixTTCTotal) + "€.")