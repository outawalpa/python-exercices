import random
import pickle

#--------------------------------------------------------------------------
# Affiche le pendu
#--------------------------------------------------------------------------
def dessin (counter):
	graphd = ['    __________', '   |  ________|', '   | |      |', '   | |      O', '   | |     /|\ ', '   | |      ^', '   | |     | |', ' __| |__', '|_______|']
	graph = ['    __________', '   |  ________|', '   | |', '   | |', '   | |', '   | |', '   | |', ' __| |__', '|_______|']
	if counter == 0: #S'il n'a plus de vie
		for i in range (0,len(graphd)):
			print(graphd[i])
	else: #Tant qu'il a des vies
		for i in range (counter, len(graph)):
			print(graph[i])

#--------------------------------------------------------------------------
# Créé le dictionnaire de mot
#--------------------------------------------------------------------------
def dictionnaire():
	array =['catastrophe', 'python', 'tomate', 'constitution', 'galipette', 'pendule', 'cardinal', 'philantrope', 'canicule', 'corruption',
			'manifestation', 'monastere', 'ornithorinque', 'parlement', 'roquette', 'protection', 'parfum', 'portable', 'croquette',
			'casquette','fondation', 'risotto', 'hachis', 'caravine', 'lunettes', 'fauteuil', 'ritournelle', 'kebab', 'chaussure']
	
	data = {'info' : array }

	with open('dicoPendu', 'wb') as file: # wb = write binary (écris par dessus) [ab = append binary (pour ajouter)]
		pickler = pickle.Pickler(file)
		pickler.dump(data) 

#--------------------------------------------------------------------------
# Le jeu du pendu
#--------------------------------------------------------------------------
def jeuPendu(soluce):
	rep = list(soluce) #Le mot à trouver
	crypte = [] #Le mot crypté

	for i in rep: crypte.append('_ ') #On crypte le mot

	print (''.join(crypte)) #Affiche proprement le mot crypté

	end = False #La partie ne s'arrête pas
	vie = 9 #Compteur de vie
	vic = 0 #Nombre d'essai
	essai = 'Lettres essayées : ' #Liste les lettres essayées

	while end == False:
		fail = True
		print('')
		ask = input('Quelle lettre ? ') #Demande la lettre
		essai += (ask + ' ') #Ajoute la lettre aux lettres essayées
		vic += 1 #Le nombre d'essai augmente de 1

		for i in range (0, len(rep)): #Pour chaque lettre du mot
			if ask == str(rep[i]): #si la lettre correspond
				fail = False #Il a trouvé
				crypte[i] = ask #Elle apparait décrypté
				print('')

		if fail == True: #Il n'a pas trouvé
			vie -= 1 #Il perd une vie
			print('')
			print('Non...')
			dessin(vie) #Affiche le pendu

		print ('')
		print (''.join(crypte)) #Affiche le mot crypté
		print ('')
		print (essai)

		if crypte == rep: #Si le mot est trouvé
			print('')
			print('BRAVO !')
			print('Tu as réussi en ' + str(vic) + ' fois')
			print('')
			return 0

		if vie == 0: #Si il n'a plus de vie
			print('')
			print('DIE !')
			print('La réponse était : ' + ''.join(rep))
			print('')
			return 0

#--------------------------------------------------------------------------
# Lance le jeu
#--------------------------------------------------------------------------
def pendu():
	with open('dicoPendu', 'rb') as file: #Lit le dictionnaire de mot
		unpickler = pickle.Unpickler(file)
		data = unpickler.load()

	array = data.get('info') #Met les mots dans un tableau
	mot = array[random.randint(0, len(array))] #Choisi un mot aléatoire
	jeuPendu(mot) #Lance le jeu du pendu avec le mot choisi

#--------------------------------------------------------------------------
# Ajoute un mot
#--------------------------------------------------------------------------
def ajoutMot():
	again = '0'

	while again != '2':
		again = '0'
		demande =[]
		test = False

		print('')
		demande.append(input('Quel est votre mot ? ')) #Demande le mot à ajouter

		with open('dicoPendu', 'rb') as file: #Lit le dictionnaire de mot
			unpickler = pickle.Unpickler(file)
			data = unpickler.load()

		array = data.get('info') #Met les mots dans un tableau

		for i in range (0, len(array)): #Pour chaque mot du tableau
			if demande[0] == array[i]: #Si le mot correspond
				test = True
				print ('')
				print('Mot déjà présent') #Indique que le mot est présent

		if test == False: #Si le mot n'est pas présent
			array.append(demande[0]) #Ajoute le mot au tableau

			data = {'info' : array }

			with open('dicoPendu', 'wb') as file: # wb = write binary (écris par dessus) [ab = append binary (pour ajouter)]
				pickler = pickle.Pickler(file)
				pickler.dump(data) 

			print ('')
			print('Mot ajouté') #Indique que le mot est ajouté

		while not (again == '1' or again == '2'): #Demande si l'user veut ajouter un autre mot
			print ('')
			print ('1: Ajouter un autre mot')
			print ('2: Retour')
			print ('')
			again = input('Que voulez vous faire ? ')

		if again == '2': return 0

#--------------------------------------------------------------------------
# Le menu
#--------------------------------------------------------------------------
def menu():
	ask = 0
	while ask != '1' or ask != '2' or ask != '3':
		print('1: Jouer')
		print('2: Ajouter un mot')
		print('3: Quitter')
		print ('')
		ask = input('Que veux tu faire : ')

		if ask == '1':
			pendu()

		elif ask == '2':
			ajoutMot()

		elif ask == '3':
			quit()

		else:
			print('')
			print ('Réponse invalide')
			print('')

#--------------------------------------------------------------------------
# Le jeu
#--------------------------------------------------------------------------
menu()