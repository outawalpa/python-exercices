relancer = 0
while relancer != 2:
	relancer = 0
	import random
	solution = random.randint(1,9)
	reponse = 00000000
	tour = 0
	affichage = ""

	for compteur in range (1,8):
		chiffre = random.randint(1,9)
		solution = str(solution) + str(chiffre)

	print (solution)

	while reponse != solution:
		print (affichage)
		affichage = ""
		reponse = int(input("Essayez de trouver les 8 chiffres (entre 1 et 9) : "))
		if len(str(reponse)) != 8:
			print("La réponse est invalide !")
		else:
			tour = tour + 1
			compteur2 = 10000000
			while compteur2 > 0.1:
				test = int(int(reponse) / compteur2) % 10
				test2 = int(int(solution) / compteur2) % 10
				if test == test2:
					affichage = affichage + " | " + str(test)
					compteur2 = compteur2 / 10
				else:
					affichage = affichage + " | " + "X"
					compteur2 = compteur2 / 10
			reponse = str(reponse)

	print ("Bravo ! La solution était " + str(solution))
	print ("Tu as réussi en " + str(tour) + " tours !")
	print ("")

	while relancer != 1 or relancer != 2:
		print ("Relancer une partie ?")
		relancer = int(input("1 = OUI | 2 = NON "))
	print ("")