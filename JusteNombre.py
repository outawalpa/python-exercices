import random
solution = random.randint(0,100)
stop = 0
coups = 0

while stop == 0:
	reponse = int(input("Essaye de trouver mon nombre compris entre 1 et 100 : "))
	if reponse == solution:
		coups = coups + 1
		print ("BRAVO ! C'était bien le nombre " + str(solution))
		print ("Tu as réussi en " + str(coups) + " coups !")
		stop = 1
	elif reponse < solution and reponse >= 0:
		coups = coups + 1
		print ("Plus grand !")
	elif reponse > solution and reponse <= 100:
		coups = coups + 1
		print ("Plus petit !")
	else:
		print ("Ce n'est pas valide")