alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
vigenere = [[] for _ in range(26)]

for i in range(26):
        for j in range(26):
            vigenere[i].append(alphabet[(i + j) % 26])

#--------------------------------------------------------------------------
# Menu
#--------------------------------------------------------------------------
def menu():
	ask = 0
	while ask != '1' or ask != '2' or ask != '3':
		displayHeader('VIGENERE APP')
		print('1: Encrypt')
		print('2: Decrypt')
		print('3: Display Vigenere table')
		print('4: Quit')
		print ('')
		ask = input('Choose a number : ')

		if ask == '1':
			encrypt()

		elif ask == '2':
			decrypt()

		elif ask == '3':
			displayTable()

		elif ask == '4':
			quit()

		else:
			print('')
			print ('Error, answer not valid.')
			print('')

#--------------------------------------------------------------------------
# Encrypt
#--------------------------------------------------------------------------
def encrypt():

	displayHeader('ENCRYPT')
	
	message = input('MESSAGE : ')
	message = message.replace(" ", "")
	message = message.upper()

	key = input('KEY : ')
	key = key.replace(" ", "")
	key = key.upper()

	result = ''

	while(len(key) < len(message)):
		key += key

	for i in range(0,len(message)):
		indexMessage = alphabet.index(message[i])
		indexKey = alphabet.index(key[i])
		result += vigenere[indexMessage][indexKey]

	print('RESULT : ' + result)
	menu()


#--------------------------------------------------------------------------
# Decrypt
#--------------------------------------------------------------------------
def decrypt():
	
	displayHeader('Decrypt')

	message = input('MESSAGE : ')
	message = message.replace(" ", "")
	message = message.upper()

	key = input('KEY : ')
	key = key.replace(" ", "")
	key = key.upper()

	result = ''

	while(len(key) < len(message)):
		key += key

	for i in range(0,len(message)):
		indexKey = alphabet.index(key[i])
		indexMessage = vigenere[indexKey].index(message[i])
		result += vigenere[0][indexMessage]

	print('RESULT : ' + result)
	menu()

#--------------------------------------------------------------------------
# Display Vigenere table
#--------------------------------------------------------------------------
def displayTable():

	displayHeader('Vigenere table')

	print('    ' + ' '.join(vigenere[0]))
	print('   ' + '-'*52)
	for row in vigenere:
		print(row[0] + ' | ' + ' '.join(row))
	menu()


#--------------------------------------------------------------------------
# Display Header of each page
#--------------------------------------------------------------------------
def displayHeader(title):
	print ('')
	print ('------------------------------------------------------------------')
	print(title)
	print ('------------------------------------------------------------------')
	print ('')
	

#--------------------------------------------------------------------------
# Launch app
#--------------------------------------------------------------------------
menu()