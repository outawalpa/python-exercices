nbrEleves = int(input("Combien y a-t-il d'élèves ? " ))
nbrNotes = int(input("Combien y a-t-il de notes ? " ))
print ("")

tableau = []
for j in range (0,nbrEleves):
	t = []
	for i in range (0,nbrNotes):
		t.append(0)
	tableau.append(t)

print ("")
somme = 0
moyClasse = 0

print(tableau)
print ("")
for j in range (0,nbrEleves):
	for i in range (0,nbrNotes):
		note = int(input("Tapez la note n° " + str(i + 1) + " de l'élève n° " + str(j + 1) + " : "))
		tableau[j][i] = note
		print (tableau)
		print ("")

for j in range (0,nbrEleves):
	print ("Eleve n° " + str(j+1), end = " ")
	print ("[", end = " ")
	for i in range (0,nbrNotes):
		if i != nbrNotes - 1:
			print (str(tableau[j][i]) + " ,", end = " ")
		else:
			print (str(tableau[j][i]), end = " ")
	print ("]")

for j in range (0,nbrEleves):
	for i in range (0,nbrNotes):
		somme = somme + tableau[i][j]
	moyenne = somme / nbrNotes
	moyClasse = moyenne / (j+1)

print("")
print ("La moyenne de la classe est égale à " + str(moyClasse))

noteSup = 0
for j in range (0,nbrEleves):
	for i in range (0,nbrNotes):
		if tableau[j][i] > moyClasse:
			noteSup += 1

print ("")
print ("Il y a " + str(noteSup) + " notes au dessus de la moyenne.")