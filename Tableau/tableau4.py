nbrNotes = int(input("Combien y a-t-il de valeurs ? " ))
tableau = [0]*nbrNotes
noteNegative = 0
notePositive = 0
somme = 0

for i in range (0,nbrNotes):
	note = int(input("Tapez la note n° " + str(i + 1) + " : "))
	tableau[i] = note
	if note < 0:
		noteNegative = noteNegative + 1
	else:
		notePositive = notePositive + 1

print ("")
print (tableau)
print("")
print ("Il y a " + str(notePositive) + " valeurs positives et " + str(noteNegative) + " valeurs négatives.")