def plateau():
	tableau = [
				[ 1, 2, 3],
				[ 4, 5, 6],
				[ 7, 8, 9]
	]

	for j in range (0,3): #Affiche le damier proprement
		for i in range (0,3):
			if i !=  (2):
				print ("[" + str(tableau[j][i]) + "]", end = "")
			else:
				print ("[" + str(tableau[j][i]) + "]")

	return tableau

def choix (pion, tableau):
	fin = False
	while fin == False:
		ask = input("Quel case ? ")
		test = ask.isdigit()
		if test == True:
			for j in range (0,3):
				for i in range(0,3):
					if tableau[j][i] == int(ask):
						tableau[j][i] = pion
						return tableau
			print ("saisie invalide")
		else:
			print ("saisie invalide")

def changeJoueur (joueur):
	if joueur == 1:
		joueur = 2
		return joueur
	else:
		joueur = 1
		return joueur

def fin(tableau, pion):
	if tableau[0][0] == pion and tableau[0][1] == pion and tableau[0][2] == pion:
		return 0
	elif tableau[1][0] == pion and tableau[1][1] == pion and tableau[1][2] == pion:
		return 0
	elif tableau[2][0] == pion and tableau[2][1] == pion and tableau[2][2] == pion:
		return 0
	elif tableau[0][0] == pion and tableau[1][0] == pion and tableau[2][0] == pion:
		return 0
	elif tableau[0][1] == pion and tableau[1][1] == pion and tableau[2][1] == pion:
		return 0
	elif tableau[0][2] == pion and tableau[1][2] == pion and tableau[2][2] == pion:
		return 0
	elif tableau[0][0] == pion and tableau[1][1] == pion and tableau[2][2] == pion:
		return 0
	elif tableau[0][2] == pion and tableau[1][1] == pion and tableau[2][0] == pion:
		return 0
	else:
		return 1



joueur = 1
fini = False
plateau = plateau ()
while fini == False:

	print("")
	if joueur == 1:
		print ("JOUEUR 1 (X)")
		pion = "X"
	else:
		print ("JOUEUR 2 (O)")
		pion ="O"

	print("")
	choix (pion, plateau)

	print("")
	for j in range (0,3):
		for i in range (0,3):
			if i !=  (2):
				print ("[" + str(plateau[j][i]) + "]", end = "")
			else:
				print ("[" + str(plateau[j][i]) + "]")

	test = fin (plateau, pion)

	if test == 0:
		print("")
		print ("JOUEUR ", joueur, " GAGNE !")
		fini = True

	joueur = changeJoueur(joueur)