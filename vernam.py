import random

#--------------------------------------------------------------------------
# Menu
#--------------------------------------------------------------------------
def menu():
	ask = 0
	while ask != '1' or ask != '2' or ask != '3':
		print ('')
		print('VERNAM APP')
		print ('')
		print('1: Encrypt')
		print('2: Decrypt')
		print('3: Quit')
		print ('')
		ask = input('Choose a number : ')

		if ask == '1':
			encrypt()

		elif ask == '2':
			decrypt()

		elif ask == '3':
			quit()

		else:
			print('')
			print ('Error, answer not valid.')
			print('')



#--------------------------------------------------------------------------
# Encrypt
#--------------------------------------------------------------------------
def encrypt():
    inputMessage = input("MESSAGE : ")

    for i in range (0,len(inputMessage)):
        if (inputMessage[i] != '0' and inputMessage[i] != '1'):
            print('Only 0 and 1 are accept.')
            menu()

    generatedKey = ''

    for i in range (0,len(inputMessage)):
        generatedKey += str(random.randint(0,1))

    print('GENERATED KEY : ' + generatedKey)

    result = ''

    for i in range(0,len(inputMessage)):
        if inputMessage[i] == generatedKey[i]:
            result += '0'
        else:
            result += '1'

    print('RESULT : ' + result)
    menu()

#--------------------------------------------------------------------------
# Decrypt
#--------------------------------------------------------------------------
def decrypt():
    inputMessage = input("MESSAGE : ")

    for i in range (0,len(inputMessage)):
        if (inputMessage[i] != '0' and inputMessage[i] != '1'):
            print('Only 0 and 1 are accept.')
            menu()

    inputKey = input("KEY : ")

    for i in range (0,len(inputKey)):
        if (inputKey[i] != '0' and inputKey[i] != '1'):
            print('Only 0 and 1 are accept.')
            menu()

    result = ''

    if (len(inputMessage) == len(inputKey)):
        for i in range(0,len(inputMessage)):
            if inputMessage[i] == inputKey[i]:
                result += '0'
            else:
                result += '1'

        print('RESULT : ' + result)
        menu()

    else:
        print('Message and key need to have the same lenght.')
        menu()

#--------------------------------------------------------------------------
# Launch app
#--------------------------------------------------------------------------
menu()