import random


def croissant ():

	array = []
	while len(array) < 1000:
		x = random.randint(0, 100000)
		if x not in array:
			array.append(x)

	fin = False
	while not fin:
		rangement = False
		for i in range (0, len(array) - 1):
			if array[i] > array[i+1]:
				array[i], array[i+1] = array[i+1], array[i]
				rangement = True
			
		if not rangement:
			fin = True

	print (array)

def dichotomie(tableauRangé):

	valeur = int(input("Chercher quelle valeur ? "))
	mini = 0
	maxi = len(tableauRangé) - 1
	taille = (maxi - mini) + 1
	while True:
		mid = mini + (taille // 2)
		val = tableauRangé[mid]
		if val == valeur:
			return print (valeur, " indéxé en position ", val)
		elif valeur < val:
			maxi = mid
		elif valeur > val:
			mini = mid
		taille = maxi - mini
		if taille <= 1:
			if tableauRangé[mini] == valeur:
				return print (valeur, " indexé en position ", mini)
			elif tableauRangé[maxi] == valeur:
				return print (valeur, " indexé en position ", maxi)

	return print ("Valeur non indexé.")
		
		




programme = -1
while programme != 0:
	print ("")
	print ("1. Croissant")
	print ("2. Dichotomie")
	print ("0. Quitter")
	programme = int(input("Quel programme ? "))
	if programme == 1:
		croissant()
	elif programme == 2:
		dichotomie([0,1,2,3,4,5,6,7,8,9,10])
	else:
		print ("")
		print ("Reponse invalide")