from Crypto.Cipher import DES3
from Crypto.Random import get_random_bytes

#--------------------------------------------------------------------------
# Menu
#--------------------------------------------------------------------------
def menu():
	ask = 0
	while ask != '1' or ask != '2' or ask != '3':
		displayHeader('3DES')
		print('1: Encrypt')
		print('2: Decrypt')
		print('3: Quit')
		print ('')
		ask = input('Choose a number : ')

		if ask == '1':
			encrypt()

		elif ask == '2':
			decrypt()

		elif ask == '3':
			quit()

		else:
			print('')
			print ('Error, answer not valid.')
			print('')

#--------------------------------------------------------------------------
# Encrypt
#--------------------------------------------------------------------------
def encrypt():

    displayHeader('3DES ENCRYPT')

    file_path = input('Where is the file to encrypt ?\n')
    key_file_path = input('Where save the key ?\n')

    # Create the random key
    while True:
        try:
            key = DES3.adjust_key_parity(get_random_bytes(24))
            break
        except ValueError:
            pass

    with open(key_file_path, 'wb') as file:
        file.write(key)

    cipher = DES3.new(key, DES3.MODE_CFB)

    # Open the input file
    with open(file_path, 'rb') as file:
        plaintext = file.read()

    if len(plaintext) % 8 != 0:
        plaintext += b'' * (8 - len(plaintext) % 8)

    # Encrypt
    ciphertext = cipher.iv + cipher.encrypt(plaintext)

    # Save the new encrypt
    with open(file_path, 'wb') as file:
        file.write(ciphertext)

    menu()
	
#--------------------------------------------------------------------------
# Decrypt
#--------------------------------------------------------------------------
def decrypt():

    displayHeader('3DES DECRYPT')

    file_path = input('Where is the file to decrypt ?\n')
    key_file_path = input('Where is the key ?\n')

    # Read the key
    with open(key_file_path, 'rb') as file:
        key = file.read()

    cipher = DES3.new(key, DES3.MODE_CFB)

    # Decrypt
    with open(file_path, 'rb') as file:
        ciphertext = file.read()

    decrypted_text = cipher.decrypt(ciphertext).rstrip(b'')

    # Save the new decrypt
    with open(file_path, 'wb') as file:
        file.write(decrypted_text[8:])

    menu()

#--------------------------------------------------------------------------
# Display Header of each page
#--------------------------------------------------------------------------
def displayHeader(title):
	print ('')
	print ('------------------------------------------------------------------')
	print(title)
	print ('------------------------------------------------------------------')
	print ('')
	

#--------------------------------------------------------------------------
# Launch app
#--------------------------------------------------------------------------
menu()