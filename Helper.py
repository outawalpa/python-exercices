import pickle
import random

def askNumber(message, exit=None, min=None, max=None):
    """ 
        ask a number\n
        message -> str\n
        exit -> str\n
        min -> int\n
        max -> int\n\n
        return int or None (if exit is set)
    """
    # Managing agrs types
    if type(message) != str: raise ValueError("message should be str")
    if exit != None and type(exit) != str: raise ValueError("exit should be str")
    if min != None and type(min) != int: raise ValueError("min should be int")
    if max != None and type(max) != int: raise ValueError("max should be int")
    if (max != None and min != None) and min > max: raise Exception("min should be smaller than max")

    # Generate and print message
    string = message
    if max != None and min != None: string += " compris entre " + str(min) + " et " + str(max)
    elif max != None: string += " inférieur à " + str(max)
    elif min != None: string += " superieur à " + str(min) 
    if exit != None: string += " (" + exit + " pour quitter)"
    print(string)

    # Main loop
    number = None
    while number == None:
        string = input("> ") # Ask a number
        if exit != None and string == exit:
            return None # if exit key exist and is typed, exit function
        try:
            number = int(string)
            if min != None and number < min: number = None
            elif max != None and number > max: number = None
            if number == None: print("Valeur incorrecte")
        except: print("Entrez un NOMBRE!")
    return number
# -------------------------
def askFloat(message, exit=None, min=None, max=None):
    """ 
        ask a float\n
        message -> str\n
        exit -> str\n
        min -> int\n
        max -> int\n\n
        return float or None (if exit is set)
    """
    # Managing agrs types
    if type(message) != str: raise ValueError("message should be str")
    if exit != None and type(exit) != str: raise ValueError("exit should be str")
    if min != None and type(min) != float: raise ValueError("min should be float")
    if max != None and type(max) != float: raise ValueError("max should be float")
    if (max != None and min != None) and min > max: raise Exception("min should be smaller than max")

    # Generate and print message
    string = message
    if max != None and min != None: string += " compris entre " + str(min) + " et " + str(max)
    elif max != None: string += " inférieur à " + str(max)
    elif min != None: string += " superieur à " + str(min) 
    if exit != None: string += " (" + exit + " pour quitter)"
    print(string)

    # Main loop
    number = None
    while number == None:
        string = input("> ") # Ask a number
        if exit != None and string == exit:
            return None # if exit key exist and is typed, exit function
        try:
            number = float(string)
            if min != None and number < min: number = None
            elif max != None and number > max: number = None
            if number == None: print("Valeur incorrecte")
        except: print("Entrez un NOMBRE!")
    return number
# -------------------------

def intArray(message, exit):
    """ 
        ask an int array\n
        message -> str\n
        exit -> str\n
        return [int]\n
    """
    print(message)
    array = []
    loop = True
    i = 1
    while loop:
        n = askNumber("Nombre numéro "+ str(i), exit)
        i += 1
        if n == None:
            loop = False
        else:
            array.append(n)
    return array
# -------------------------
def floatArray(message, exit):
    """ 
        ask a float array\n
        message -> str\n
        exit -> str\n
        return [float]\n
    """
    print(message)
    array = []
    loop = True
    i = 1
    while loop:
        n = askFloat("Nombre numéro "+ str(i), exit)
        i += 1
        if n == None:
            loop = False
        else:
            array.append(n)
    return array
# -------------------------
def stringArray(message, exit):
    """ 
        ask a str array\n
        message -> str\n
        exit -> str\n
        return [str]\n
    """
    print(message)
    array = []
    loop = True
    i = 1
    while loop:
        n = input("Chaîne numéro "+ str(i) + " ("+ exit + " pour quitter): ")
        i += 1
        if n == exit:
            loop = False
        else:
            array.append(n)
    return array
# -------------------------
def save(fileName, data):
    with open(fileName, 'wb') as file:
        pickler = pickle.Pickler(file)
        pickler.dump(data)
# -------------------------
def load(fileName):
    with open(fileName, 'rb') as file:
        unpickler = pickle.Unpickler(file)
        data = unpickler.load()
    return data
# -------------------------
def randomValue(array):
    i = random.randint(0, len(array))
    return array[i]
# -------------------------
def offuscate(string, discoveredChar):
    newString = ""
    for c in string:
        if c in discoveredChar:
            newString += c + " "
        else:
            newString += "_ "
    return newString
# -------------------------

if __name__ == '__main__':
    s = "toto"
    x = offuscate(s, ["t"])
    print(x)